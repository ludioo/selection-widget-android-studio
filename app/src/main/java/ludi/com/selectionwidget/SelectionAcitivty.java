package ludi.com.selectionwidget;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

public class SelectionAcitivty extends AppCompatActivity implements TextWatcher {

    AutoCompleteTextView edit;
    String[] items = {
            "Artificial Intelligence",
            "Augmented Reality",
            "Finite State Machine",
            "Augmented Virtuality",
            "Virtual Reality",
            "Mixed Reality",
            "Augmented Learning",
            "Genetic Algorithm"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        edit = findViewById(R.id.edit);
        edit.addTextChangedListener(this);
        edit.setAdapter(new ArrayAdapter<String>(this,
                R.layout.custom_selection, items));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }


}
